#!/bin/bash
# Return codes
ERR_PARSING=100
ERR_NO_INPUT_FILENAME=101

# Minimum debug level for certain message types
debug_level_max=100
debug_level_dd=25
debug_level_ffmpeg=20
debug_level_sox=$debug_level_ffmpeg
debug_level_secondary_functions=10
debug_level_primary_functions=5

debug_level=$debug_level_primary_functions

# Some sane defaults
sox_options_default="-t raw -r 48000 -e signed -b 24 -c 2 -x"
ffmpeg_options_default="-c:v libx264 -c:a aac -crf 0 -vf fps=50 -pix_fmt yuv420p"

# The most basic help
usage () {
    echo "$0 <SETTINGS>"
    echo "The only required setting is"
    echo "-i <INPUT_FILENAME>"
    echo "Optional settings are"
    echo "-en <SOX_EFFECT_NAME>"
    echo "-en defaults to bass"
    echo "-eba <SOX_EFFECT_BASE_AMOUNT>"
    echo "-eba defaults to 0.01"
    echo "-so <SOX_OPTIONS>"
    echo "-so defaults to \"$sox_options_default\""
    echo "-so don't forget to quote the list of options"
    echo "-t <TAG>"
    echo "-t default is empty"
    echo "-t <TAG> is used as a final suffix for the resulting file before the file extension"
    echo "-dl <DEBUG_LEVEL>"
    echo "-dl default is $debug_level_primary_functions"
    echo "-dl <DEBUG_LEVEL> is an integer between 0 and $debug_level_max"
    echo "-sea"
    echo "-sea causes using unsigned integers for the sox effects"
    echo "-sea does not work properly at the moment"
    echo "-dr"
    echo "-do not remove the temporary directory full of bmp files"
    echo "-uet <TMP_DIR>"
    echo "-use existing temp_dir <TMP_DIR> (not yet working)"
}

# Parsing arguments
while [[ $# -gt 0 ]] ; do
key="$1"
    case $key in
        -i)
        input_filename="$2"
        shift # past argument
        shift # past value
        ;;
        -en)
        sox_effect_name="$2"
        shift # past argument
        shift # past value
        ;;
        -eba)
        sox_effect_base_amount="$2"
        shift # past argument
        shift # past value
        ;;
        -so)
        sox_options="$2"
        shift # past argument
        shift # past value
        ;;
        -t)
        tag="$2"
        shift # past argument
        shift # past value
        ;;
        -dl)
        debug_level="$2"
        shift # past argument
        shift # past value
        ;;
        -sea)
        socks_effect_absolute="1"
        shift # past argument
        ;;
        -dr)
        dont_remove="1"
        shift # past argument
        ;;
        -uet)
        use_existing_temp="$2"
        shift # past argument
        shift # past value
        ;;
        *)    # unknown option
        usage
        exit $ERR_PARSING
        shift # past argument
        ;;
    esac
done

if [ ! -f "$input_filename" ] ; then
    usage
    exit $ERR_NO_INPUT_FILENAME
fi

pwd_was=$(pwd)
tmp_dir=$(mktemp -d -p ./)
cd $tmp_dir
filename=$(basename -- "$input_filename")
filename="${filename%.*}"
[[ "${sox_options}" != "" ]] || sox_options=$sox_options_default
[[ "${sox_effect_name}" != "" ]] || sox_effect_name="treble"
[[ "${sox_effect_base_amount}" != "" ]] || sox_effect_base_amount="0.001"
[[ "${tag}" != "" ]] && filename_ending="${sox_effect_name}_${sox_effect_base_amount}_${tag}" || filename_ending="${sox_effect_name}_${sox_effect_base_amount}"

# frames per beat
fbp=25
# either 1 or -1, so that the amount of the sox effect increases or decreases
direction=1

debug_log () {
    prio=$1
    shift
    if [[ "$#" -gt 0 ]] ; then
        [[ "$debug_level" -ge "$prio" ]] && echo $(for i in $(seq 1 $prio) ; do echo -n . ; done) "$@"
    else
        while read line ; do
            [[ "$debug_level" -ge "$prio" ]] && echo $(for i in $(seq 1 $prio) ; do echo -n . ; done) "$line"
        done < /dev/stdin
    fi
}

# This would expect the base file name (no numbering, no extensions) and will create a .bmp_headerless and a .bmp_header file in the current directory
bmp_header_detacher () {
    debug_log $debug_level_secondary_functions Beginning separating the bmp headers from the bmp data of $1:
    offset=$((16#$(head -c 12 "${1}.bmp" | tail -c 2 | xxd -e | tr -s " " | cut -d " " -f 2)))
    dd if="${1}.bmp" of="${1}.bmp_header" bs="$offset" count=1 2>&1 | debug_log $debug_level_dd
    dd if="${1}.bmp" of="${1}.bmp_headerless" bs="$offset" skip=1 2>&1 | debug_log $debug_level_dd
    debug_log $debug_level_secondary_functions Finished separating the bmp headers from the bmp data of $1. 
}

# This would expect the paths to the header, the headerless soxed bitmap and where the result is going to save to (it overwrites) and reattaches the bmp headers
bmp_header_reattacher () {
    debug_log $debug_level_secondary_functions Beginning reattaching the bmp headers of the bmp data of $1:
    offset=$((16#$(head -c 12 "${1}" | tail -c 2 | xxd -e | tr -s " " | cut -d " " -f 2)))
    echo 'Going to cat '$1' '$2' | dd of="'${3}'" 2>&1 | debug_log $debug_level_dd' | debug_log $debug_level_dd
    cat $1 $2 | dd of="${3}" 2>&1 | debug_log $debug_level_dd
    debug_log $debug_level_secondary_functions Finished reattaching the bmp headers of the bmp data of $1. 
}

# This would expect an amount of glitching (signed integer) and the base file name (no extensions)
frame_glitcher () {
    sox_effect_current_amount=$(bc <<< $sox_effect_base_amount*$1)
    sox_effect="$sox_effect_name $sox_effect_current_amount"
    debug_log $debug_level_sox Soxing like sox $sox_options "$2.bmp_headerless" $sox_options "$2.bmp_headerless_soxed" $sox_effect:
    sox $sox_options "$2.bmp_headerless" $sox_options "$2_soxed.bmp_headerless" $sox_effect 2>&1 | debug_log $debug_level_sox
    debug_log $debug_level_sox Soxing done. 
}

frame_extractor () {
debug_log $debug_level_primary_functions Beginning extracting frames from source video: 
debug_log $debug_level_primary_functions Running ffmpeg -nostdin -y -i "${pwd_was}/${input_filename}" "$ffmpeg_options_default" "${filename}_temp.mp4"
ffmpeg_options="-nostdin -y -i ${pwd_was}/${input_filename} $ffmpeg_options_default ${filename}_temp.mp4"
ffmpeg $ffmpeg_options < /dev/null 2>&1 | debug_log $debug_level_ffmpeg
ffmpeg -nostdin -y -i "${filename}_temp.mp4" "${filename}_%04d.bmp" < /dev/null 2>&1 | debug_log $debug_level_ffmpeg
debug_log $debug_level_primary_functions Finished extracting frames from source video. 
}
# First we separate the video into frames
frame_extractor

loop_bmp_detacher_soxer_reattacher () {
# Invoke the chain for each bmp already generated: 
debug_log $debug_level_primary_functions Beginning the loop to separate all headers:

# TODO this if doesn't work
# TODO maybe something like
# -(fpb / 2) to -1 to 1 to (fpb/2)
# and if abs then
# 1 .. fpb
declare -i minj
declare -i maxj
declare -i dirdir
if [[ "$socks_effect_absolute" -eq "1" ]] ; then
    minj=1
    maxj=$(($fbp*2))
    dirdir=1
else
    minj=$((-$fbp/2))
    maxj=$(($fbp/2))
    dirdir=-1
fi
j=1
for i in {1..9999} ; do
    if [[ -f "${filename}_$(printf %04d $i).bmp" ]] ; then
        bmp_header_detacher "${filename}_$(printf %04d $i)"
        if [[ "$j" -le "$maxj" ]] && [[ "$j" -ge "$minj" ]] ; then
            if [[ "$j" -eq "$(($dirdir*$direction))" ]] ; then
                let j=$direction
            else
                let j=$j+$direction
            fi
        else
            let direction=-1*$direction
            let j=$j+$direction
        fi
        echo j is $j and direction is $direction | debug_log $debug_level_secondary_functions
        frame_glitcher $j "${filename}_$(printf %04d $i)"
        bmp_header_reattacher "${filename}_$(printf %04d $i).bmp_header" "${filename}_$(printf %04d $i)_soxed.bmp_headerless" "${filename}_$(printf %04d $i)_${filename_ending}.bmp"
    else
        break
    fi
done
debug_log $debug_level_primary_functions Finished the loop to separate all headers. 
}
loop_bmp_detacher_soxer_reattacher

assemble_video_from_frames () {
debug_log $debug_level_primary_functions Beginning moving back to original directory, uploading, finishing up and removing remnant temp data: 
debug_log $debug_level_secondary_functions Beginning video reassembly: 
ffmpeg_options="-nostdin -y -r 50 -i ${filename}_%04d_${filename_ending}.bmp $ffmpeg_options_default ${filename}_${filename_ending}_noaudio.mp4"
debug_log $debug_level_secondary_functions Running ffmpeg $ffmpeg_options
ffmpeg $ffmpeg_options < /dev/null 2>&1 | debug_log $debug_level_ffmpeg
echo PIPESTATUS[0] is "${PIPESTATUS[0]}" and PIPESTATUS[1] is "${PIPESTATUS[0]}" | debug_log $debug_level_secondary_functions
ffmpeg_options="-nostdin -y -i ${filename}_${filename_ending}_noaudio.mp4 -i ${filename}_temp.mp4 $ffmpeg_options_default ${filename}_${filename_ending}.mp4"
debug_log $debug_level_secondary_functions Running ffmpeg -nostdin -y -i "${filename}_${filename_ending}_noaudio.mp4" -i "${filename}_temp.mp4" "$ffmpeg_options_default" "${filename}_${filename_ending}.mp4"
ffmpeg $ffmpeg_options < /dev/null 2>&1 | debug_log $debug_level_ffmpeg
debug_log $debug_level_secondary_functions Finished video reassembly. 
#mv "${filename}_${filename_ending}_noaudio.mp4" $pwd_was
mv "${filename}_${filename_ending}.mp4" $pwd_was
#mv ${filename}_temp.mp4 $pwd_was
cd $pwd_was
#scp -q "${filename}_${filename_ending}.mp4" pi@kopame.com:/usr/local/nginx/html/dmqk/andro/
#ssh -q pi@kopame.com "sudo chown www-data:www-data /usr/local/nginx/html/dmqk/andro/\"${filename}_${filename_ending}.mp4\"" < /dev/null
[[ "$dont_remove" -eq "1" ]] || rm -fr $tmp_dir
#echo "https://kopame.com/dmqk/andro/${filename}_${filename_ending}.mp4"
debug_log $debug_level_primary_functions Finished moving back to original directory, finishing up and removing remnant temp data. 
}
assemble_video_from_frames

# stuff to be implemented some day
# an example on how to reencode with lower (like 10 mbps) bitrate so that the final result is playable in normal devices
# ffmpeg -nostdin -y -i IGORRR_-_VERY_NOISE_treble_0.1.mp4 -c:v h264 -b:v 10000k -pass 1 -an -f h264 /dev/null
# ffmpeg -nostdin -i IGORRR_-_VERY_NOISE_treble_0.1.mp4 -c:v h264 -b:v 10000k -pass 2 -c:a aac -b:a 192k IGORRR_-_VERY_NOISE_treble_0.1_reencoded.mp4

